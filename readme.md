# Exemplo de requisição

## POST http://localhost:8081/aplicacao

### Request
```
{
	"cliente": {
		"cpf": "123.123.123-12"
	},
	"produto": {
		"id": 1
	},
	"transacao": {
		"valor": 50000 
	}
}
```

> Para que essa requisição seja bem sucedida, todos os microsserviços (cliente, aplicacao, produto e eureka) devem estar disponíveis.
