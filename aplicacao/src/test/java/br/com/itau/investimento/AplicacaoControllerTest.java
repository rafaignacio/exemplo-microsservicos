package br.com.itau.investimento;

import static org.mockito.Mockito.when;
import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.services.AplicacaoService;
import br.com.itau.investimento.viewobjects.Investimento;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AplicacaoControllerTest {

	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	AplicacaoService aplicacaoService;
	
	@Test
	public void buscarClientes() throws Exception {
		Aplicacao aplicacao = new Aplicacao();
		aplicacao.setId(1);
		
		Investimento investimento = new Investimento();
		investimento.setAplicacao(aplicacao);
		
		when(aplicacaoService.criar(any(Investimento.class))).thenReturn(Optional.of(investimento));
		
		mockMvc.perform(post("/aplicacao")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content("{\"aplicacao\": {\"id\": 1} }")
			)
			.andExpect(content().string(containsString("1")));
	}
}
